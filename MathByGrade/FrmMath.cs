﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MathByGrade
{
	public partial class FrmMath : Form
	{
		public FrmMath()
		{
			InitializeComponent();
			cbG1.DisplayMember = "Description";
			cbG1.Items.AddRange(ProblemsByGrade.Problems[1].ToArray<object>());
			cbG2.DisplayMember = "Description";
			cbG2.Items.AddRange(ProblemsByGrade.Problems[2].ToArray<object>());
			cbG3.DisplayMember = "Description";
			cbG3.Items.AddRange(ProblemsByGrade.Problems[3].ToArray<object>());
			cbG4.DisplayMember = "Description";
			cbG4.Items.AddRange(ProblemsByGrade.Problems[4].ToArray<object>());
			btnPrint.Enabled = false;
		}

		private void btnPrint_Click(object sender, EventArgs e)
		{
			var destination = "C:/temp";
			var cb = tabGrade.SelectedTab.Controls.OfType<CheckedListBox>().FirstOrDefault();
			var generators = cb.CheckedItems.OfType<IProblemGenerator>();
			int x = 0;
			var sheet = new StringBuilder();
			var answerSheet = new StringBuilder();
			var rnd = new Random();
			while (x < numProblems.Value)
			{
				var col = 1;
				while (col++ <= numCols.Value && x < numProblems.Value)
				{
					var problem = generators.OrderBy(o => rnd.NextDouble()).First().GenerateProblem();
					sheet.AppendFormat("{0}),\"{1}\",", ++x, problem.Text);
					answerSheet.AppendFormat("{0}),\"{1}\",", x, problem.Answer);
				}
				sheet.AppendLine();
				answerSheet.AppendLine();
			}

			if (!Directory.Exists(destination))
			{
				Directory.CreateDirectory(destination);
			}

			File.WriteAllText(
				Path.Combine(destination, "questions.csv"), 
				sheet.ToString());

			File.WriteAllText(
				Path.Combine(destination, "answers.csv"), 
				answerSheet.ToString());
		}

		private void tabGrade_SelectedIndexChanged(object sender, EventArgs e)
		{
			var cb = tabGrade.SelectedTab.Controls.OfType<CheckedListBox>().FirstOrDefault() ;
			btnPrint.Enabled = cb != null && cb.CheckedItems.Count > 0;
		}

		private void CheckedListBox_SelectedValueChanged(object sender, EventArgs e)
		{
			btnPrint.Enabled = (sender as CheckedListBox)?.CheckedItems.Count > 0;

		}
	}
}
